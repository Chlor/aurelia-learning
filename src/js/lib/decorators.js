import {defer, typeOf} from './utils'

const finalizePromises = (promises = [], type, value) => {
  let p
  while (p = promises.shift()) {
    p[type](value)
  }
}

/**
 * @description perform an async operation once, queue all other calls
 * until first one resolves
 * @decorator
 * @param target
 * @param key
 * @param desc
 */
export const singular = (target, key, desc) => {
  let promises = [],
      running  = false,
      orig     = desc.value

  desc.value = async function (...args) {
    if (running) {
      let deferred = defer()
      promises.push(deferred)
      return deferred.promise
    }
    running = true
    try {
      let res = await Promise.resolve(orig.apply(this, args))
      finalizePromises(promises, 'resolve', res)
      return res
    } catch (err) {
      finalizePromises(promises, 'reject', err)
      throw err
    } finally {
      running = false
    }
  }
}

/**
 * Return a descriptor removing the value and returning a getter
 * The getter will return a .bind version of the function
 * and memoize the result against a symbol on the instance
 */
export const bind = (target, key, descriptor) => {
  let fn = descriptor.value

  if (typeof fn !== 'function') {
    throw new Error(`@autobind decorator can only be applied to methods not: ${typeof fn}`)
  }

  return {
    configurable: true,
    get() {
      if (this === target.prototype || this.hasOwnProperty(key)) {
        return fn
      }

      let boundFn = fn.bind(this)
      Object.defineProperty(this, key, {
        value: boundFn,
        configurable: true,
        writable: true
      })
      return boundFn
    }
  }
}

export const serialize = (...fields) => target => {
  target.prototype.serialize = function () {
    const target = Object.create(null)
    Object.keys(this).forEach(key => {
      if (fields.includes(key)) {
        switch(typeOf(this[key])) {
          case 'object':
            target[key] = Object.create(null)
            Object.keys(this[key]).forEach(key2 => {
              let curr = this[key][key2]
              target[key][key2] = curr.serialize ? curr.serialize() : curr
            })
            break
          case 'array':
            target[key] = []
            this[key].forEach(elem => {
              target[key].push(elem.serialize ? elem.serialize() : elem)
            })
            break
          default:
            target[key] = this[key]
        }
      }
    })
    return target
  }
}
