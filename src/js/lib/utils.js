export const sleep = n => new Promise(resolve => setTimeout(resolve, n * 1e3))

export const typeOf = any =>
  Object.prototype.toString.call(any).replace(/.*\s|]/g, '').toLowerCase()

export const nullify = o =>
  Object.keys(o).forEach(key => {
    if (typeOf(o[key]) !== 'function') {
      o[key] = null
    }
  })

export const defer = () => {
  const deferred = {},
        promise  = new Promise((resolve, reject) => {
          deferred.resolve = resolve
          deferred.reject = reject
        })
  deferred.promise = promise
  return deferred
}

export const checkResponse = res => {
  if (!res.ok) {
    throw res
  }
}
