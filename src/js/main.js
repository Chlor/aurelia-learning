import {bootstrap} from 'aurelia-bootstrapper-webpack'

import 'aurelia-validation/dist/commonjs/index'
import 'bootstrap/scss/bootstrap.scss'
import 'bootstrap/dist/js/bootstrap.min.js'
import 'font-awesome/css/font-awesome.min.css'
import '../css/style.scss'

bootstrap(aurelia => {
  aurelia.use
    .standardConfiguration()
    .developmentLogging()
    //.plugin('aurelia-dialog', config => {
    //  config.useDefaults()
    //})
    .plugin('aurelia-validation')
    .plugin('globalResources.js')
  aurelia.start().then(() => aurelia.setRoot('viewModels/app', document.body))
})
