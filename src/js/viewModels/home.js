import {inject} from 'aurelia-framework'
import UserService from '../services/User'

@inject(UserService)
export class Home {
  message = 'test'
  inputVal = null
  i = 0

  person = {firstName: 'Wade', middleName: 'Owen', lastName: 'Watts'}

  constructor(userService) {
    Object.assign(this, {userService})
  }

  async activate() {
    this.user = this.userService.getUser()
  }

  log() {
    console.log(this.inputVal)
  }

  test() {
    return this.i
  }

  inc() {
    this.i++
  }

}
