import {inject} from 'aurelia-framework'

import {Basic} from './basic.js'
import {TestCase as TestCaseService} from '../../../services/TestCase'

import {bind} from '../../../lib/decorators'

@inject(Basic, TestCaseService)
export class New {

  constructor(parent, testCaseService) {
    Object.assign(this, {parent, testCaseService})
  }
}
