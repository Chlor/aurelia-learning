import {inject} from 'aurelia-framework'
import {Router} from 'aurelia-router'
import {TestCase as TestCaseService} from '../../../services/TestCase'
import {default as TestCaseModel, Variant} from '../../../models/TestCase'
import {bind} from '../../../lib/decorators'

@inject(Router, TestCaseService)
export class Basic {

  testCase = new TestCaseModel()

  constructor(parentRouter, testCaseService) {
    Object.assign(this, {parentRouter, testCaseService})
  }

  configureRouter(config, router) {
    config.map([
      {
        route: ['', 'new'],
        name: 'new',
        title: 'New',
        moduleId: './new',
        nav: true
      }
    ])
    this.router = router
  }

  async activate({id}) {
    id
      ? this.testCase.set(await this.testCaseService.get(id))
      : this.testCase = new TestCaseModel().addVariant()
  }

  async next() {
    try {
      await this.testCase.validate()
      let res = await this.testCaseService.save(this.testCase)
      this.parentRouter.navigate(`test-cases/basic/${res.id}`)
    } catch (err) {
      console.log(err)
    }
  }
}
