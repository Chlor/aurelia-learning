import {inject} from 'aurelia-framework'
import UserModel from '../models/User'
import UserService from '../services/User'
const {READ_ONLY, USER, ADMIN } = UserModel

export class App {
  configureRouter(config, router) {

    config.addPipelineStep('authorize', AuthStep)

    config.title = 'Aurelia'
    config.options.pushState = true
    config.map([
      {
        route: ['home', ''],
        name: 'home',
        moduleId: './home',
        nav: true,
        title: 'Home',
        auth: true
      },
      {
        route: 'login',
        name: 'login',
        moduleId: './login',
        nav: false,
        title: 'Login'
      },
      {
        route: ['users/list/:id', 'users/list'],
        name :'users/list',
        nav: false,
        title: 'List',
        moduleId: './users/list',
        settings: {
          allowed: ADMIN
        },
        auth: true
      },
      {
        route: 'users/add',
        name: 'users/add',
        nav: false,
        title: 'Add',
        moduleId: './users/add-edit',
        settings: {
          allowed: ADMIN
        },
        auth: true
      },
      {
        route: 'users/edit/:id',
        name: 'users/edit',
        title: 'Edit',
        moduleId: './users/add-edit',
        settings: {
          allowed: ADMIN
        },
        auth: true
      },
      {
        route: ['test-cases/basic/:id', 'test-cases/basic'],
        name: 'basic',
        title: 'Basic',
        moduleId: './testCases/basic/basic',
        auth: true
      }
    ])
    this.router = router
  }
}

@inject(UserService)
class AuthStep {
  constructor(userService) {
    this.user = userService
  }

  run(instruction, next) {
    if (instruction.config.auth && !this.user.isLoggedIn) {
      instruction.router.navigate('/login')
      return next.cancel()
    }
    let {settings} = instruction.config,
        allowed = settings && settings.allowed

    if (allowed && !this.user.user.is(allowed)) {
      instruction.router.navigate('home')
      return next.cancel()
    }

    return next()
  }
}
