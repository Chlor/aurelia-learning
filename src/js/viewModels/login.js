import {inject} from 'aurelia-framework'
import {Router} from 'aurelia-router'

import {Validation, ensure} from 'aurelia-validation'

import UserService from '../services/User'

@inject(Router, Validation, UserService)
export class Login {

  username = ''
  password = ''
  error = null

  constructor(router, validation, userService) {
    Object.assign(this, {router, userService})
    this.validation = validation.on(this)
      .ensure('username').isNotEmpty()
      .ensure('password').isNotEmpty()
  }

  async login() {
    try {
      await this.validation.validate()
    } catch (err) {
      this.error = {
        message: 'Missing login or password',
        type: 'warning'
      }
      return
    }
    try {
      await this.userService.login(this.username, this.password)
      this.router.navigate('/home')
    } catch (err) {
      this.error = {message: err, type: 'danger'}
    }
  }
}
