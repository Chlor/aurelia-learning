import {bindable, inject} from 'aurelia-framework'
import {Router} from 'aurelia-router'

import Global from '../services/Global'
import UserService from '../services/User'
import UserModel from '../models/User'

@inject(Router, Global, UserService)
export class NavBar {

  @bindable router = null

  constructor(router, global, userService) {
    Object.assign(this, {router, global, userService})
    this.user = userService.getUser()
    this.UserModel = UserModel
  }

  /**
   * because this viewModel isn't under governance of router, activate doesn't
   * get called
   */
  submit() {

  }

  async logout() {
    await this.userService.logout()
    this.router.navigate('/login')
  }
}
