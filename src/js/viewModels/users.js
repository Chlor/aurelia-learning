import {inject} from 'aurelia-framework';
import {HttpClient} from 'aurelia-fetch-client';

@inject(HttpClient)
export class Users {

  heading = 'Github Users'
  users = []

  constructor(http) {
    this.http = http.configure(config => {
      config
        .useStandardConfiguration()
        .withBaseUrl('https://api.github.com')
    })
  }

  async activate() {
    this.users = await (await this.http.fetch('/users')).json()
  }
}
