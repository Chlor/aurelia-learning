import {inject, useView} from 'aurelia-framework'
import {Router} from 'aurelia-router'

import {Validation, ValidationResult} from 'aurelia-validation'

import {bind} from '../../lib/decorators'

import UsersService from '../../services/Users'
import UserModel from '../../models/User'

@inject(Router, Validation, UsersService)
@useView('./users/add-edit.html')
export class AddEdit {

  user = new UserModel()
  title = ''
  roles = UserModel.roles
  edit = false

  constructor(router, validation, usersService) {
    Object.assign(this, {router, usersService})
    this.validation = validation.on(this)
      .ensure('user.name').isNotEmpty().hasLengthBetween(3, 12)
      .ensure('user.surname').isNotEmpty().hasLengthBetween(3, 12)
      .ensure('user.login').isNotEmpty().hasLengthBetween(4, 12)
      .ensure('user.email').isNotEmpty().isEmail()
  }

  async activate({id}) {
    if (id) {
      this.edit = true
      //this.validation.ensure('user.password')
      try {
        this.user.set(await this.usersService.get(id))
      } catch (err) {
        this.error = {
          message: err,
          type: 'danger'
        }
      }
    } else {
      this.validation.ensure('user.password').isNotEmpty().hasLengthBetween(5, 12)
      this.edit = false
      this.user.destroy()
    }
    this.title = `${this.edit ? 'Edit' : 'Add'} user`
  }

  async submit() {
    try {
      await this.validation.validate()
      let {_id} = await this.usersService.save(this.user)
      this.router.navigate(`users/list/${_id}`)
    } catch (err) {
      if (err instanceof ValidationResult) {
        let badFields = Object.keys(err.properties).reduce((p, key) => {
          let curr = err.properties[key]
          if (!curr.isValid) {
            p.push(`${key}: ${curr.message}`)
          }
          return p
        }, []).join('<br>')
        this.error = {
          message: badFields,
          type: 'warning'
        }
      } else {
        this.error = {
          message: err,
          type: 'danger'
        }
      }
    }
  }

  @bind
  dismissAlert() {
    this.error = null
  }


}
