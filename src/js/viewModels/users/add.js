import {inject, useView} from 'aurelia-framework'
import UsersService from '../../services/Users'
import UserModel from '../../models/User'

import {bind} from '../../lib/decorators'

@inject(UsersService)
@useView('./users/add-edit.html')
export class Add {

  title = 'Add User'
  user = new UserModel
  roles = UserModel.roles
  error = null

  constructor(usersService) {
    this.usersService = usersService
  }

  async submit() {
    try {
      let res = await this.usersService.save(this.user)
      this.error = {
        message: `User ${res.name} ${res.surname} (${res.login}) saved successfully`,
        type: 'success'
      }
      this.user.destroy()
    } catch (err) {
      this.error = {
        message: err,
        type: 'danger'
      }
    }
  }
}
