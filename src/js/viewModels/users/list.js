import {inject, LogManager} from 'aurelia-framework'
import UsersService from '../../services/Users'

const logger = LogManager.getLogger('viewModels/users/list')

@inject(UsersService)
export class List {

  users = []
  sortData = {
    key: 'login',
    dir: 1
  }

  tableHeader = ['login', 'name', 'surname', 'email', 'role']

  constructor(usersService) {
    Object.assign(this, {usersService})
  }

  async activate({id}) {
    if (id) {
      this.id = id
    }
    this.users = await this.usersService.getList()
  }

  async remove(user) {
    try {
      await this.usersService.remove(user)
      this.users = this.users.filter(u => u._id !== user._id)
    } catch (err) {
      logger.warn(err)
    }
  }

  sort(key) {
    if (key !== this.sortData.key) {
      this.sortData.key = key
    } else {
      this.sortData.dir ^= 1
    }
  }

}
