import {bindable} from 'aurelia-framework'

/**
 * @fixme - dismissable needs some work
 */
export class Alert {
  @bindable type = 'warning'
  @bindable message = 'abc'
  @bindable dismissable = false
  @bindable ondismiss = () => {}

  visible = true

  dismiss() {
    this.ondismiss()
    this.visible = false
  }

  messageChanged() {
    this.visible = !!this.message
  }

}
