import {inject, LogManager} from 'aurelia-framework'
import {HttpClient} from 'aurelia-fetch-client'
import UserService from '../services/User'

import {singular} from '../lib/decorators'
import {checkResponse} from '../lib/utils'

import {default as TestCaseModel} from '../models/TestCase'

@inject(UserService)
export class TestCase {
  constructor(userService) {
    this.http = new HttpClient().configure(config => {
      config
        .useStandardConfiguration()
        .withBaseUrl('http://localhost:8080/api/v1')
        .withDefaults({
          headers: {
            authorization: `Bearer ${userService.getUser().token}`
          }
        })
      //.withInterceptor(basic)
    })
  }

  @singular
  async get(id) {
    let res = await (await this.http.fetch(`/test-case/${id}`)).json()
    checkResponse(res)
    return res.data
  }

  new() {
    return new TestCaseModel().addVariant()
  }

  @singular
  async save(testCase) {
    let res = await (await this.http.fetch('/test-case', {
      method: 'POST',
      body: JSON.stringify(testCase.serialize())
    })).json()
    checkResponse(res)
    return res.data
  }
}
