import {inject, LogManager} from 'aurelia-framework'
import {HttpClient, json} from 'aurelia-fetch-client'

import Global from '../services/Global'

import {sleep, nullify} from '../lib/utils'
import {singular} from '../lib/decorators'
import {basic} from '../lib/interceptors'
import UserModel from '../models/User'

const logger = LogManager.getLogger('services/User')

@inject(Global)
export default class User {

  isLoggedIn = false
  user = new UserModel()

  constructor(global) {
    this.http = new HttpClient().configure(config => {
      config
        .useStandardConfiguration()
        .withBaseUrl('http://localhost:8080/auth')
        //.withInterceptor(basic)
    })
    Object.assign(this, {global})
  }

  setUser(token) {
    let user = this.deserialize(token)
    if (!user || user.exp * 1e3 < Date.now()) {
      this.logout()
    }
    this.user.set(Object.assign({}, {token, ...user}))
    this.global.showNav = true
    this.isLoggedIn = true
  }

  getUser() {
    if (this.isLoggedIn) {
      return this.user
    }
    let token = this.getToken()
    if (token) {
      this.setUser(token)
    }
    return this.user
  }

  setToken(token) {
    localStorage.setItem('token', token)
  }

  getToken() {
    return localStorage.getItem('token')
  }

  deserialize(token) {
    try {
      return JSON.parse(atob(token.split('.')[1]))
    } catch (err) {
      logger.error(err)
    }
  }

  @singular
  async login(username, password) {
    let res = await this.http.fetch('/login', {
        method: 'POST',
        body: JSON.stringify({
          login: username,
          password
        })
      })
    res = await res.json()
    if (!res.ok) {
      throw res.error
    }
    let token = res.data.token
    this.setUser(token)
    this.setToken(token)
    return this.user
  }

  async logout() {
    this.isLoggedIn = false
    this.global.showNav = false
    this.user.destroy()
    localStorage.removeItem('token')
  }
}
