import {inject} from 'aurelia-framework'

import {HttpClient} from 'aurelia-fetch-client'

import {basic} from '../lib/interceptors'

import {singular} from '../lib/decorators'
import {checkResponse} from '../lib/utils'
import UserService from './User'
import UserModel from '../models/User'

@inject(UserService)
export default class Users {
  constructor(userService) {
    this.http = new HttpClient().configure(config => {
      config
        .useStandardConfiguration()
        .withBaseUrl('http://localhost:8080/api/v1')
        .withDefaults({
          headers: {
            authorization: `Bearer ${userService.getUser().token}`
          }
        })
      //.withInterceptor(basic)
    })
  }

  @singular
  async getList() {
    let res = await (await this.http.fetch('/users')).json()
    checkResponse(res)
    return res.data
  }

  @singular
  async get(id) {
    let res = await(await this.http.fetch(`/users/${id}`)).json()
    checkResponse(res)
    return res.data
  }

  @singular
  async save(user) {
    let res = await(await this.http.fetch('/users', {
      method: 'POST',
      body: user.json()
    })).json()
    checkResponse(res)
    return res.data
  }

  @singular
  async remove(user) {
    let res = await(await this.http.fetch(`/users/${user._id}`, {
      method: 'DELETE'
    })).json()
    checkResponse(res)
    return
  }
}
