export class JsonValueConverter {
  toView(value, indent = 2) {
    try {
      return JSON.stringify(value, null, indent)
    } catch (e) {
      return value
    }
  }
}
