export class CapitalizeValueConverter {
  toView(elem) {
    return elem[0].toUpperCase() + elem.slice(1)
  }
}
