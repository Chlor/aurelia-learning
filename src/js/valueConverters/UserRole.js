import UserModel from '../models/User'

export class UserRoleValueConverter {
  toView(value) {
    return UserModel.roleMap[UserModel.roles.indexOf(+value)]
  }
}
