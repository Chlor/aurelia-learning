import {typeOf} from '../lib/utils'

const filter = re => elem => {
  switch (typeOf(elem)) {
    case 'string':
      return re.test(elem)
    case 'number':
      return re.test('' + elem)
    case 'array':
      return elem.some(filter(re))
    case 'object':
      return Object.keys(elem).some(key => filter(re)(elem[key]))
    default:
      return true
  }
}

export class FilterValueConverter {
  toView(data, re) {
    return data.filter(filter(new RegExp(re, 'i')))
  }
}
