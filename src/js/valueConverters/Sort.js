const lower = any => {
  return ('' + (any || '')).toLowerCase()
}

export class SortValueConverter {
  toView(data, key, dir) {
    return data.sort((a, b) => {
      let one = lower(a[key]), two = lower(b[key])
      return dir ? one > two : one < two
    })
  }
}
