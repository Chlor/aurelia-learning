import {Container} from 'aurelia-dependency-injection'
import {nullify} from '../lib/utils'
import {serialize} from '../lib/decorators'
import {Validation} from 'aurelia-validation'

const letters = 'abcdefghijklmnopqrstuwxyz'.split('')

const validation = new Container().get(Validation)

@serialize('_id', 'name', 'url', 'id', 'variants', 'status')
export default class TestCase {

  name = ''
  url = ''
  variants = []
  status
  id

  constructor(data) {
    this.set(data)
    this.validation = validation.on(this)
      .ensure('name').isNotEmpty()

    this.variants.json = () => {
      return this.variants.map(v => v.json())
    }
    return this
  }

  set(data = {}) {
    Object.assign(this, data)
    if (data.variants) {
      this.variants = data.variants.map(v => new Variant(v))
    }
    return this
  }

  validate() {
    return Promise.all([this.validation.validate(), ...this.variants.map(v => v.validation.validate())])
  }

  destroy() {
    nullify(this)
    return this
  }

  addVariant() {
    if (!this.variants) {
      this.variants = []
    }
    this.variants.push(new Variant({
      name: `Variant ${letters[this.variants.length].toUpperCase()}`,
      url: ''
    }))
    return this
  }

  removeVariant(index) {
    this.variants.splice(index, 1)
    return this
  }
}

@serialize('uuid', 'name', 'url')
export class Variant {
  name = ''
  url = ''
  constructor(data) {
    Object.assign(this, data)
    this.validation = validation.on(this)
      .ensure('name').isNotEmpty()
      .ensure('url').isNotEmpty().isURL()
  }
}
