import {nullify, sleep} from '../lib/utils'

const READ_ONLY = 0b001
const USER      = 0b010
const ADMIN     = 0b100
const roles = [READ_ONLY, USER, ADMIN]
const roleMap = ['read_only', 'user', 'admin']

export default class User {

  static READ_ONLY = READ_ONLY
  static USER = USER
  static ADMIN = ADMIN
  static roles = roles
  static roleMap = roleMap

  constructor(data) {
    this.set(data)
  }

  is(role = null) {
    return role ? !!(role & this.role) : true
  }

  destroy() {
    nullify(this)
  }

  set(data) {
    Object.assign(this, data)
  }

  json(options = {omit: []}) {
    return JSON.stringify(this)
  }

}
