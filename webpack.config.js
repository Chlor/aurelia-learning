const path                 = require('path'),
      AureliaWebpackPlugin = require('aurelia-webpack-plugin'),
      ProvidePlugin        = require('webpack/lib/ProvidePlugin')
module.exports = {
  entry: {
    main: ['./src/js/main']
  },
  output: {
    path: './dist',
    filename: '[name].js',
    publicPath: 'dist/'
  },
  devServer: {
    host: 'localhost',
    port: 8888,
    inline: true,
    historyApiFallback: {
      index: 'index.html'
    }
  },
  plugins: [
    new AureliaWebpackPlugin({
      src: path.resolve('./src/js'),
      includeSubModules: [
        //{moduleId: 'aurelia-dialog'},
        {moduleId: 'aurelia-validation'}
      ]
    }),
    new ProvidePlugin({
      $: 'jquery',
      jQuery: 'jquery',
      Tether: 'tether'
    })
  ],
  module: {
    loaders: [
      {
        test: /\.js$/,
        loader: 'babel',
        exclude: /node_modules/
      },
      {test: /\.scss$/, loaders: ['style', 'css', 'sass']},
      {test: /\.css?$/, loader: 'style!css', exclude: /aurelia-dialog/},
      //{test: /\.css?$/, loader: 'raw', include: /aurelia-dialog/},
      {test: /\.html$/, loader: 'html'},
      {test: /\.(png|gif|jpg)$/, loader: 'url?limit=8192'},
      {
        test: /\.woff2(\?v=[0-9]\.[0-9]\.[0-9])?$/,
        loader: 'url?limit=10000&minetype=application/font-woff2'
      },
      {
        test: /\.woff(\?v=[0-9]\.[0-9]\.[0-9])?$/,
        loader: 'url?limit=10000&minetype=application/font-woff'
      },
      {test: /\.(ttf|eot|svg)(\?v=[0-9]\.[0-9]\.[0-9])?$/, loader: 'file'}
    ]
  }, /*,
   resolve: {
   extensions: ['', '.js', '.html']
   },
   resolveLoader: {
   modulesDirectories: [path.resolve('./node_modules')]
   },*/
  devtool: 'sourcemap'
}
